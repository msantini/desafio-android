package br.com.marlon.desafioconcrete.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import br.com.marlon.desafioconcrete.R;
import br.com.marlon.desafioconcrete.model.Repositorio;

/**
 * Created by Marlon on 23/01/2018.
 */

public class RepoAdapter extends RecyclerView.Adapter<RepoAdapter.PersonViewHolder> {

    private ArrayList<Repositorio> repositorio;
    public Context context;

    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        Typeface QsBold = Typeface.createFromAsset(itemView.getResources().getAssets(), "Quicksand-Bold.ttf");
        Typeface QsRegular = Typeface.createFromAsset(itemView.getResources().getAssets(), "Quicksand-Regular.ttf");

        TextView tvUsername;
        TextView tvNome;
        TextView tvNomeRepo;
        TextView tvDescricaoRepo;
        TextView tvFork;
        TextView tvStar;
        ImageView ivFoto;



        PersonViewHolder(View itemView) {
            super(itemView);
            itemView.setClickable(true);

            tvNome = itemView.findViewById(R.id.tvNome);
            tvNome.setTypeface(QsBold);
            tvUsername = itemView.findViewById(R.id.tvUsername);
            tvUsername.setTypeface(QsRegular);
            tvNomeRepo = itemView.findViewById(R.id.tvNomeRepo);
            tvNomeRepo.setTypeface(QsRegular);
            tvDescricaoRepo = itemView.findViewById(R.id.tvDescricaoRepo);
            tvDescricaoRepo.setTypeface(QsRegular);
            tvFork = itemView.findViewById(R.id.tvFork);
            tvFork.setTypeface(QsBold);
            tvStar = itemView.findViewById(R.id.tvStar);
            tvStar.setTypeface(QsBold);
            ivFoto = itemView.findViewById(R.id.ivFoto);


        }
    }

    public RepoAdapter(Context context, ArrayList<Repositorio> repositorio) {
        this.context = context;
        this.repositorio = repositorio;
    }

    @Override
    public RepoAdapter.PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_repositorio, viewGroup, false);
        RepoAdapter.PersonViewHolder pvh = new RepoAdapter.PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RepoAdapter.PersonViewHolder personViewHolder, int position) {
        personViewHolder.tvNome.setText(repositorio.get(position).getUsername());

        personViewHolder.tvNomeRepo.setText(repositorio.get(position).getName());
        personViewHolder.tvDescricaoRepo.setText(repositorio.get(position).getDescription());

        personViewHolder.tvFork.setText(Integer.toString(repositorio.get(position).getForks_count()));
        personViewHolder.tvStar.setText(Integer.toString(repositorio.get(position).getStargazers_count()));

        Glide.with(context).load(repositorio.get(position).getAvatar_url()).apply(RequestOptions.circleCropTransform().skipMemoryCache(true)).into(personViewHolder.ivFoto);

    }

    @Override
    public int getItemCount() {
        return repositorio.size();

    }
}
