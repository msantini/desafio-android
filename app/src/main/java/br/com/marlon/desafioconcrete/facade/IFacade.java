package br.com.marlon.desafioconcrete.facade;

import android.content.Context;

import java.util.ArrayList;

import br.com.marlon.desafioconcrete.model.Repositorio;


/**
 * @author Marlon Santini
 */
public interface IFacade {

    /**
     * Metodos UserRequest
     */

    ArrayList<Repositorio> listaRepositorio();


    /**
     * Metodos PullRequestRequest
     */

    ArrayList<Repositorio> listaPullRequest(String full_name);



    /**
     * Outros Metodos
     */

    boolean verificarConexao(Context context);



}
