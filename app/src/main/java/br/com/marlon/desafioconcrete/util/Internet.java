package br.com.marlon.desafioconcrete.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Marlon on 22/01/2018.
 */
public class Internet
{
    private Context context;

    public Internet(Context context) {
        this.context = context;
    }

    public boolean verificarConexao()
    {
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}