package br.com.marlon.desafioconcrete.request;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import br.com.marlon.desafioconcrete.model.Repositorio;
import br.com.marlon.desafioconcrete.util.Constants;

/**
 * Created by Marlon on 22/01/2018.
 */

public class RepoRequest extends BaseRequest {

    private String strJson;
    private JSONObject jsonReturnApi;
    private Gson gson;

    public ArrayList<Repositorio> listaRepositorio(){

        try {

            gson = new Gson();
            setMethod(Method.GET);
            setUrl(Constants.URL);

            strJson = this.execute(this).get();
            jsonReturnApi = new JSONObject(strJson);

            if (Integer.valueOf(jsonReturnApi.get("total_count").toString()) == 0) {

                return null;
            }
            else {

                JSONArray jArray = jsonReturnApi.getJSONArray("items");
                ArrayList<Repositorio> repositorioArrayList = new ArrayList<>();

                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jsonObject = jArray.getJSONObject(i);

                    Repositorio repositorio = new Repositorio();
                    repositorio.setName(jsonObject.getString("name"));
                    repositorio.setDescription(jsonObject.getString("description"));
                    repositorio.setFull_name(jsonObject.getString("full_name"));
                    repositorio.setForks_count(jsonObject.getInt("forks_count"));
                    repositorio.setStargazers_count(jsonObject.getInt("stargazers_count"));
                    repositorio.setUsername(jsonObject.getJSONObject("owner").getString("login"));
                    repositorio.setAvatar_url(jsonObject.getJSONObject("owner").getString("avatar_url"));


                    repositorioArrayList.add(repositorio);
                }

                return repositorioArrayList;
            }
        }
        catch (Exception e)
        {
            return null;
        }
    }
}
