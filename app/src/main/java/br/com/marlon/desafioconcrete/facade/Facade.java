package br.com.marlon.desafioconcrete.facade;

import android.content.Context;

import java.util.ArrayList;

import br.com.marlon.desafioconcrete.model.Repositorio;
import br.com.marlon.desafioconcrete.request.PullRequestRequest;
import br.com.marlon.desafioconcrete.request.RepoRequest;
import br.com.marlon.desafioconcrete.util.Internet;


/**
 * @author Marlon Santini
 */
public class Facade implements IFacade {


    /**
     * Metodos RepoRequest
     */

    @Override
    public ArrayList<Repositorio> listaRepositorio() {
        RepoRequest repoRequest = new RepoRequest();
        return repoRequest.listaRepositorio();
    }

    /**
     * Metodos PullRequestRequest
     */

    @Override
    public ArrayList<Repositorio> listaPullRequest(String full_name) {
        PullRequestRequest pullRequestRequest = new PullRequestRequest();
        return pullRequestRequest.listaPullRequest(full_name);
    }


    /**
     * Outros Metodos
     */

    @Override
    public boolean verificarConexao(Context context) {
        Internet internet = new Internet(context);
        return internet.verificarConexao();
    }

}
