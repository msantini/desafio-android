package br.com.marlon.desafioconcrete.util;

/**
 * Created by Marlon on 22/01/2018.
 */

public class Constants {

    public static final String URL = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1";

    public static final String URLPULL = "https://api.github.com/repos/";

    public static final String URLFULLNAME(String full_name)
    {
        return URLPULL + full_name + "/pulls";
    }
}
