package br.com.marlon.desafioconcrete.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import br.com.marlon.desafioconcrete.R;
import br.com.marlon.desafioconcrete.model.Repositorio;

/**
 * Created by Marlon on 24/01/2018.
 */

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.PersonViewHolder> {

    private ArrayList<Repositorio> repositorio;
    public Context context;

    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        Typeface QsBold = Typeface.createFromAsset(itemView.getResources().getAssets(), "Quicksand-Bold.ttf");
        Typeface QsRegular = Typeface.createFromAsset(itemView.getResources().getAssets(), "Quicksand-Regular.ttf");

        TextView tvUsername;
        TextView tvDescricaoPull;
        TextView tvTitle;
        ImageView ivFoto;



        PersonViewHolder(View itemView) {
            super(itemView);
            itemView.setClickable(true);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvTitle.setTypeface(QsBold);
            tvUsername = itemView.findViewById(R.id.tvUsername);
            tvUsername.setTypeface(QsRegular);
            tvDescricaoPull = itemView.findViewById(R.id.tvDescricaoPull);
            tvDescricaoPull.setTypeface(QsRegular);
            ivFoto = itemView.findViewById(R.id.ivFoto);


        }
    }

    public PullRequestAdapter(Context context, ArrayList<Repositorio> repositorio) {
        this.context = context;
        this.repositorio = repositorio;
    }

    @Override
    public PullRequestAdapter.PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_pullrequest, viewGroup, false);
        PullRequestAdapter.PersonViewHolder pvh = new PullRequestAdapter.PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PullRequestAdapter.PersonViewHolder personViewHolder, int position) {
        personViewHolder.tvUsername.setText(repositorio.get(position).getUsername());

        personViewHolder.tvTitle.setText(repositorio.get(position).getTitle());
        personViewHolder.tvDescricaoPull.setText(repositorio.get(position).getBody());

        Glide.with(context).load(repositorio.get(position).getAvatar_url()).apply(RequestOptions.circleCropTransform().skipMemoryCache(true)).into(personViewHolder.ivFoto);

    }

    @Override
    public int getItemCount() {
        return repositorio.size();

    }
}
