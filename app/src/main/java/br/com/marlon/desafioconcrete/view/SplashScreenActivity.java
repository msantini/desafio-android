package br.com.marlon.desafioconcrete.view;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import br.com.marlon.desafioconcrete.R;

public class SplashScreenActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run() {

                finish();
                Intent intent = new Intent();
                intent.setClass(SplashScreenActivity.this, HomeActivity.class);
                startActivity(intent);

            }
        }, SPLASH_TIME_OUT);
    }
}
