package br.com.marlon.desafioconcrete.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.marlon.desafioconcrete.R;
import br.com.marlon.desafioconcrete.adapter.PullRequestAdapter;
import br.com.marlon.desafioconcrete.facade.Facade;
import br.com.marlon.desafioconcrete.model.Repositorio;
import br.com.marlon.desafioconcrete.util.DividerItemDecoration;
import br.com.marlon.desafioconcrete.util.RecyclerItemClickListener;
import br.com.marlon.desafioconcrete.util.Util;

public class PullRequestActivity extends AppCompatActivity {

    private RecyclerView rvPullRequest;
    private LinearLayoutManager llm;
    private TextView toolbar_title;
    private PullRequestAdapter mAdapter;
    private ArrayList<Repositorio> repositorio;
    private String full_name;
    private String title;

    private LinearLayout llEmptyState;
    private TextView tvOpen;
    private TextView tvqtdOpen;
    public LinearLayoutManager.SavedState savedState;



    private Facade facade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);
        Util.setCtxAtual(this);

        Typeface QsBold = Typeface.createFromAsset(getResources().getAssets(), "Quicksand-Bold.ttf");
        Typeface QsRegular = Typeface.createFromAsset(getResources().getAssets(), "Quicksand-Regular.ttf");

        // Handle Toolbar
        Toolbar toolbar = findViewById(R.id.tb_main);
        toolbar.setTitle("");
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorAccent));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(QsRegular);

        rvPullRequest = findViewById(R.id.rvPullRequest);
        llEmptyState = findViewById(R.id.llEmptyState);
        tvOpen = findViewById(R.id.tvOpen);
        toolbar_title.setTypeface(QsBold);
        tvqtdOpen = findViewById(R.id.tvqtdOpen);
        toolbar_title.setTypeface(QsBold);


        facade = new Facade();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            full_name = extras.getString("full_name");
            title = extras.getString("title");
            toolbar_title.setText(title);
        }

        if (facade.verificarConexao(getBaseContext())) {
            Util.AtivaDialogHandler(2, "", "Carregando...");
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        repositorio = facade.listaPullRequest(full_name);
                        mAdapter = new PullRequestAdapter(getBaseContext(), repositorio);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    verificaRepo();
                                    llm = new LinearLayoutManager(getBaseContext());
                                    rvPullRequest.setLayoutManager(llm);
                                    rvPullRequest.setHasFixedSize(true);
                                    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvPullRequest.getContext(),
                                            llm.getOrientation());
                                    rvPullRequest.addItemDecoration(dividerItemDecoration);
                                    rvPullRequest.setAdapter(mAdapter);
                                    Util.AtivaDialogHandler(5, "", "");
                                    rvPullRequest.addOnItemTouchListener(new RecyclerItemClickListener(getBaseContext(), rvPullRequest, new RecyclerItemClickListener.OnItemClickListener() {

                                                @Override
                                                public void onItemClick(View view, int position) {
                                                    if(facade.verificarConexao(getBaseContext())){
                                                        Uri uri = Uri.parse(repositorio.get(position).getHtml_url());
                                                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                                        startActivity(intent);
                                                    }else {
                                                        new AlertDialog.Builder(PullRequestActivity.this)
                                                                .setCancelable(false)
                                                                .setMessage("Por favor, verifique sua conexao com a internet.")
                                                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                                    public void onClick(DialogInterface dialog, int which) {

                                                                    }
                                                                }).show();
                                                    }

                                                }

                                                @Override
                                                public void onLongItemClick(View view, int position) {

                                                }
                                            })
                                    );
                                }
                            });
                    } catch (Exception e) {
                        Util.AtivaDialogHandler(5, "", "");
                    }
                }
            }).start();
        } else {
            new AlertDialog.Builder(PullRequestActivity.this)
                    .setCancelable(false)
                    .setMessage("Por favor, verifique sua conexao com a internet.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).show();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        savedState = (LinearLayoutManager.SavedState) rvPullRequest.getLayoutManager().onSaveInstanceState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void verificaRepo(){
        if(repositorio == null){
            llEmptyState.setVisibility(View.VISIBLE);
            rvPullRequest.setVisibility(View.GONE);
        }else{
            tvqtdOpen.setText(Integer.toString(repositorio.size()));
        }
    }
}
