package br.com.marlon.desafioconcrete.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;


import java.util.ArrayList;

import br.com.marlon.desafioconcrete.R;
import br.com.marlon.desafioconcrete.adapter.RepoAdapter;
import br.com.marlon.desafioconcrete.facade.Facade;
import br.com.marlon.desafioconcrete.model.Repositorio;
import br.com.marlon.desafioconcrete.util.DividerItemDecoration;
import br.com.marlon.desafioconcrete.util.RecyclerItemClickListener;
import br.com.marlon.desafioconcrete.util.Util;

public class HomeActivity extends AppCompatActivity {

    private RecyclerView rvRepositorios;
    private LinearLayoutManager llm;
    private TextView toolbar_title;
    private RepoAdapter mAdapter;
    private String full_name;
    private String title;
    private ArrayList<Repositorio> repositorio;


    private Facade facade;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Util.setCtxAtual(this);

        Typeface QsRegular = Typeface.createFromAsset(getResources().getAssets(), "Quicksand-Regular.ttf");


        // Handle Toolbar
        Toolbar toolbar = findViewById(R.id.tb_main);
        toolbar.setTitle("");
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorAccent));
        setSupportActionBar(toolbar);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(QsRegular);

        facade = new Facade();

        if (facade.verificarConexao(getBaseContext())) {
            Util.AtivaDialogHandler(2, "", "Carregando Repositórios...");
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        repositorio = facade.listaRepositorio();
                        mAdapter = new RepoAdapter(getBaseContext(), repositorio);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    rvRepositorios = findViewById(R.id.rvRepositorios);
                                    llm = new LinearLayoutManager(getBaseContext());
                                    rvRepositorios.setLayoutManager(llm);
                                    rvRepositorios.setHasFixedSize(true);
                                    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvRepositorios.getContext(),
                                            llm.getOrientation());
                                    rvRepositorios.setAdapter(mAdapter);
                                    Util.AtivaDialogHandler(5, "", "");
                                    rvRepositorios.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                        @Override
                                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                            super.onScrolled(recyclerView, dx, dy);

                                        }
                                    });

                                    rvRepositorios.addOnItemTouchListener(new RecyclerItemClickListener(getBaseContext(), rvRepositorios, new RecyclerItemClickListener.OnItemClickListener() {

                                                @Override
                                                public void onItemClick(View view, int position) {
                                                    full_name = repositorio.get(position).getFull_name();
                                                    title = repositorio.get(position).getName();
                                                    Intent intent = new Intent(getBaseContext(), PullRequestActivity.class);
                                                    Bundle extras = new Bundle();
                                                    intent.putExtra("full_name", full_name);
                                                    intent.putExtra("title", title);
                                                    intent.putExtras(extras);
                                                    startActivity(intent);
                                                }

                                                @Override
                                                public void onLongItemClick(View view, int position) {

                                                }
                                            })
                                    );
                                }
                            });
                    } catch (Exception e) {
                        Util.AtivaDialogHandler(5, "", "");
                    }
                }
            }).start();
        } else {
            new AlertDialog.Builder(HomeActivity.this)
                    // Set Dialog Title
                    //.setTitle("")
                    .setCancelable(false)
                    // Set Dialog Message
                    .setMessage("Por favor, verifique sua conexao com a internet.")
                    // Positive button
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        finish();
                        }
                    }).show();
        }

    }
}
