package br.com.marlon.desafioconcrete.request;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import br.com.marlon.desafioconcrete.model.Repositorio;
import br.com.marlon.desafioconcrete.util.Constants;

/**
 * Created by Marlon on 24/01/2018.
 */

public class PullRequestRequest extends BaseRequest {

    private String strJson;
//    private JSONObject jsonReturnApi;
    private JSONArray jsonReturnApi;
    private Gson gson;

    public ArrayList<Repositorio> listaPullRequest(String full_name){

        try {

            gson = new Gson();
            setMethod(Method.GET);
            setUrl(Constants.URLFULLNAME(full_name));

            strJson = this.execute(this).get();
            jsonReturnApi = new JSONArray(strJson);

            if (jsonReturnApi.length() == 0) {

                return null;
            }
            else {

                //JSONArray jArray = jsonReturnApi.getJSONArray(Integer.parseInt("array"));
                ArrayList<Repositorio> repositorioArrayList = new ArrayList<>();

                for (int i = 0; i < jsonReturnApi.length(); i++) {
                    JSONObject jsonObject = jsonReturnApi.getJSONObject(i);

                    Repositorio repositorio = new Repositorio();
                    repositorio.setBody(jsonObject.getString("body"));
                    repositorio.setTitle(jsonObject.getString("title"));
                    repositorio.setUsername(jsonObject.getJSONObject("user").getString("login"));
                    repositorio.setAvatar_url(jsonObject.getJSONObject("user").getString("avatar_url"));
                    repositorio.setHtml_url(jsonObject.getString("html_url"));


                    SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                    Date date = dt.parse(jsonObject.getString("created_at"));
                    SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-mm-dd");
                    repositorio.setCreated_at(dt1.format(date));


//                    repositorio.setForks_count(jsonObject.getInt("forks_count"));
//                    repositorio.setStargazers_count(jsonObject.getInt("stargazers_count"));


                    repositorioArrayList.add(repositorio);
                }

                return repositorioArrayList;
            }
        }
        catch (Exception e)
        {
            return null;
        }
    }
}
